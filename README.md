Attachments
===========
Yii2 module for attachments

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist mitrii/yii2-attachments "*"
```

or add

```
"mitrii/yii2-attachments": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \mitrii\attachments\AutoloadExample::widget(); ?>```